#ifmuse-composer

##注意：
1. 本项目为开放源代码项目，严禁掺杂涉及公司内部业务逻辑的代码；
2. 本项目的部分组件依赖Laravel5.3以上版本，但未在composer.json中声明，以后将开新项目将这部分组件移动到那里并声明依赖关系；
3. 本项目依赖PHP5.6及更高版本；

## 类：Kuandd\Common\MultiActionProxy
依赖：Laravel5.3及以上版本
作用：基于Laravel的多路由批量调用

### 方法：getInstance(int $maxCalls = 10)
调用方式：static
作用：修改最多调用次数
参数：
1. $maxCalls int 最多调用次数

返回值：Kuandd\Common\MultiActionProxy 本类实例

### 方法：setCallLimit(int $maxCalls)
调用方式：$this
作用：修改最多调用次数
参数：
1. $maxCalls int 最多调用次数

### 方法：verify(Illuminate\Http\Request $request)
调用方式：$this
作用：验证请求
参数：
1. $request Illuminate\Http\Request Laravel框架请求实例

### 方法：invoke(Illuminate\Http\Request $request, RouteCollection $listRoute)
调用方式：$this
作用：批量执行路由
参数：
1. $request Illuminate\Http\Request Laravel框架请求实例
2. $listRoute Illuminate\Routing\RouteCollection Laravel框架路由集合

返回值：array 批量执行结果

---

##**快递鸟&聚合数据物流信息查询**

1.注册服务提供者

```php
\Kuandd\Common\KDNiaoServerProvider::class,
\Kuandd\Common\JuheExpressServerProvider::class,
```

2. 创建配置文件

> php artisan vendor:publish

3.添加环境变量

> `.env`中添加如下配置:

```

####快递鸟物流信息配置####

#快递鸟 用户ID
KDNIAO_BUSINESS_ID=
#快递鸟 API key
KDNIAO_API_KEY=
#日志文件地址
KDNIAO_LOG_FILE=


####聚合数据物流信息配置####

#聚合数据物流 Appkey
JUHEEXP_APP_KEY=
#日志文件地址
JUHEEXP_LOG_FILE=

```

## 钉钉开放平台

### 获取日志数据

```
    //范例
    public function handle()
    {
        $ding   = new DingTalk;
        $max    = 10;
        $cursor = 0;
        $size   = 100;
        $output = fopen('php://output', 'w');

        for ($offset = 0;$offset < $max;$offset ++) {

            $result = $ding->listReport(self::CORP, [
                'start_time'    => strtotime('- 1days') * 1000,
                'end_time'      => time() * 1000,
                'template_name' => '工作日报',
                'cursor'        => $cursor,
                'size'          => $size,
            ]);
            $cursor = $result['result']['next_cursor'];

            foreach ($result['result']['data_list'] as $record) {

                fputcsv($output, [
                    $record['creator_name'],
                    $record['contents'][0]['value'],
                    $record['contents'][1]['value'],
                    $record['contents'][2]['value'],
                    $record['contents'][3]['value'],
                    $record['remark'],
                    date('Y-m-d H:i:s', floor($record['create_time'] / 1000)),
                ]);
            }

            if (false === $result['result']['has_more']) {

                break;
            }
        }

        fclose($output);
    }

```

### 发送工作通知

```
    //范例
    public function handle()
    {

        $ding   = new DingTalk;
        $ding->conversationSend(self::CORP, [
            'agent_id'      => env('DING_AGENT_ID'),
            'userid_list'   => 5,
            'msg'           => [
                'msgtype'   => 'text',
                'text'      => [
                    'content'   => 'test ' . date('Y-m-d H:i:s')
                ],
            ],
        ]);
    }
```
