<?php
namespace   Kuandd\Common;

use GuzzleHttp\ClientInterface;
use \GuzzleHttp\Client;
use Cache;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;

class   DingTalk implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * API前缀
     */
    const   PREFIX_API                  = 'https://oapi.dingtalk.com/';

    /**
     * 钉钉AccessToken缓存键配置
     */
    const   CACHEKEY_SNSACCESSTOKEN     = 'cachekey.ding_sns_access_token';

    /**
     * 钉钉AccessToken寿命 2h
     */
    const   CACHEEXPIRE_SNSACCESSTOKEN  = 120;

    /**
     * 钉钉AccessToken缓存键配置
     */
    const   CACHEKEY_ACCESSTOKEN        = 'cachekey.ding_access_token';

    /**
     * 钉钉AccessToken寿命 2h
     */
    const   CACHEEXPIRE_ACCESSTOKEN     = 120;


    /**
     * 钉钉SNSToken信息缓存配置
     */
    const   CACHEKEY_SNSTOKENINFO       = 'cachekey.ding_sns_token_info';

    /**
     * 钉钉SNSToken缓存寿命
     */
    const   CACHEEXPIRE_SNSTOKENINFO    = 120;

    /**
     * 钉钉SNS AccessToken缓冲
     */
    private $_snsAccessToken            = [];

    /**
     * 钉钉SNS AccessToken缓冲挂钟计时
     */
    private $_wallTimeSnsAccessToken    = [];

    /**
     * 钉钉AccessToken缓冲
     */
    private $_accessToken               = [];

    /**
     * 钉钉AccessToken缓冲挂钟计时
     */
    private $_wallTimeAccessToken       = [];

    /**
     *  Http instance.
     */
    private $_httpClient;

    /**
     *  http handler
     */
    private $_httpHandler;

    /**
     * 获取访问令牌
     *
     * @param   string  $corp   企业令牌键
     * @return  string          令牌
     */
    public  function getAccessToken($corp)
    {
        //如果缓冲存在AccessToken则直接返回
        //如果程序执行时间较长获取时间差超过AccessToken寿命则重新获取
        if (
            isset($this->_accessToken[$corp]) &&
            isset($this->_wallTimeAccessToken[$corp]) &&
            (time() - $this->_wallTimeAccessToken[$corp]) < (self::CACHEEXPIRE_ACCESSTOKEN * 60)
        ) {

            return  $this->_accessToken[$corp];
        }

        $this->_wallTimeAccessToken[$corp]  = time();
        $this->_accessToken[$corp]          = Cache::remember(
            config(self::CACHEKEY_ACCESSTOKEN) . '_' . $corp,
            self::CACHEEXPIRE_ACCESSTOKEN,
            function () use ($corp) {

                $corpid = config('ding.corpList.' . $corp . '.corpId');
                $corpsecret = config('ding.corpList.' . $corp . '.corpSecret');

            $tokenInfo  = $this->_get('/gettoken', [
                'corpid'        => $corpid,
                'corpsecret'    => $corpsecret,
            ]);

            return  $tokenInfo['access_token'];
        });

        return  $this->_accessToken[$corp];
    }

    /**
     * 创建审批流
     *
     * @param   string  $corp   企业令牌键
     * @param   array   $params 审批参数
     *                          process_code:审批id;
     *                          originator_user_id:发起用户id;
     *                          dept_id:发起团队id;
     *                          approvers:审批人id列表;
     *                          form_component_values:审批表单数据;
     * @return  array           是否成功
     */
    public  function createProcessInstance($corp, $params)
    {
        return  $this->_post('/topapi/processinstance/create', [
            'access_token'  => $this->getAccessToken($corp),
        ],[
            'process_code'          => $params['process_code'],
            'originator_user_id'    => $params['originator_user_id'],
            'dept_id'               => $params['dept_id'],
            'approvers'             => $params['approvers'],
            'form_component_values' => $params['form_component_values'],
        ]);
    }

    /**
     * 获取日志
     *
     * @param   string  $corp   企业令牌键
     * @param   array   $params 查询参数 cursor:起始位置|默认0;size:返回数量|默认10
     * @return  array           是否成功
     */
    public  function listReport($corp, $params)
    {
        return  $this->_post('/topapi/report/list', [
            'access_token'  => $this->getAccessToken($corp),
        ],[
            'start_time'        => $params['start_time'],
            'end_time'          => $params['end_time'],
            'template_name'     => $params['template_name'],
            'cursor'            => isset($params['cursor']) ? (int) $params['cursor']   : 0,
            'size'              => isset($params['size'])   ? (int) $params['size']     : 10,
        ]);
    }

    /**
     * 发送工作通知
     *
     * @param   string  $corp   企业令牌键
     * @param   array   $params 查询参数
                                agent_id:应用代理id;
                                userid_list:接收者的用户userid列表，最大列表长度：20;
                                dept_id_list:接收者的部门id列表，最大列表长度：20;
                                msg:消息内容，具体见“消息类型与数据格式”。最长不超过2048个字节。重复消息内容当日只能接收一次。
     * @return  array           是否成功
     */
    public  function conversationSend($corp, $params)
    {
        return  $this->_post('/topapi/message/corpconversation/asyncsend_v2', [
            'access_token'  => $this->getAccessToken($corp),
        ], array_filter([
            'agent_id'      => $params['agent_id'],
            'userid_list'   => isset($params['userid_list'])    ? $params['userid_list']    : '',
            'dept_id_list'  => isset($params['dept_id_list'])   ? $params['dept_id_list']   : '',
            'msg'           => $params['msg'],
        ]));
    }

    /**
     * 查询工作通知发送情况
     *
     * @param   string  $corp   企业令牌键
     * @param   array   $params 查询参数
                                agent_id:应用代理id;
                                task_id:通知任务id;
     * @return  array           是否成功
     */
    public  function conversationStatus($corp, $params)
    {
        return  $this->_post('/topapi/message/corpconversation/getsendprogress', [
            'access_token'  => $this->getAccessToken($corp),
        ],[
            'agent_id'  => $params['agent_id'],
            'task_id'   => $params['task_id'],
        ]);
    }

    /**
     * 根据unionid获取用户信息
     *
     * @param   string  $corp       企业令牌键
     * @param   string  $unionId    用户唯一标识
     * @return  array               用户信息
     */
    public  function getUserByUnionId($corp, $unionId)
    {
        return  $this->_get('/user/getUseridByUnionid', [
            'access_token'  => $this->getAccessToken($corp),
            'unionid'       => $unionId,
        ]);
    }

    /**
     * 根据userid获取用户信息
     *
     * @param   string  $corp   企业令牌键
     * @param   string  $userId 用户ID
     * @return  array           用户信息
     */
    public  function getUserByUserId($corp, $userId)
    {
        return  $this->_get('/user/get', [
            'access_token'  => $this->getAccessToken($corp),
            'userid'        => $userId,
        ]);
    }

    /**
     * 获取普通用户access_token
     *
     * @param   string  $corp   企业令牌键
     * @return mixed
     */
    public function getSnsAccessToken($corp)
    {
        //如果缓冲存在AccessToken则直接返回
        //如果程序执行时间较长获取时间差超过AccessToken寿命则重新获取
        if (
            isset($this->_snsAccessToken[$corp]) &&
            isset($this->_wallTimeSnsAccessToken[$corp]) &&
            (time() - $this->_wallTimeSnsAccessToken[$corp]) < (self::CACHEEXPIRE_SNSACCESSTOKEN * 60)
        ) {

            return  $this->_snsAccessToken[$corp];
        }

        $this->_wallTimeSnsAccessToken[$corp]   = time();
        $this->_snsAccessToken[$corp]           = Cache::remember(
            config(self::CACHEKEY_SNSACCESSTOKEN) . '_' . $corp,
            self::CACHEEXPIRE_SNSACCESSTOKEN,
            function () use ($corp) {

            $tokenInfo  = $this->_get('/sns/gettoken', [
                'appid'     => config('ding.corpList.' . $corp . '.appId'),
                'appsecret' => config('ding.corpList.' . $corp . '.appSecret'),
            ]);

            return  $tokenInfo['access_token'];
        });

        return  $this->_snsAccessToken[$corp];
    }

    /**
     * 获取用户授权的持久授权码
     *
     * @param   string  $corp   企业令牌键
     * @param   string  $tmpAuthCode    临时授权码
     * @return  array                   永久授权信息
     */
    public  function getSnsPersistentCode($corp, $tmpAuthCode)
    {
        return  $this->_post('/sns/get_persistent_code', [
            'access_token'  => $this->getSnsAccessToken($corp),
        ], [
            'tmp_auth_code' => $tmpAuthCode,
        ]);
    }

    /**
     * 获取用户的SNS令牌
     *
     * @param   string  $corp   企业令牌键
     * @param   array   $persistentInfo 永久授权信息
     * @return  string                  SNSToken
     */
    public  function getSnsToken($corp, $persistentInfo)
    {
        return  Cache::remember(
            config(self::CACHEKEY_SNSTOKENINFO) . $persistentInfo['persistent_code'],
            self::CACHEEXPIRE_SNSTOKENINFO,
            function () use ($persistentInfo, $corp) {

            $tokenInfo  = $this->_post('/sns/get_sns_token', [
                'access_token'  => $this->getSnsAccessToken($corp),
            ], [
                'openid'            => $persistentInfo['openid'],
                'persistent_code'   => $persistentInfo['persistent_code'],
            ]);

            return  $tokenInfo['sns_token'];
        });
    }

    /**
     * 获取用户SNS信息
     *
     * @param   string  $corp   企业令牌键
     * @param   array   $persistentInfo 永久授权信息
     * @return  array                   用户信息
     */
    public  function getSnsUserInfo($corp, $persistentInfo)
    {
        return  $this->_get('/sns/getuserinfo', [
            'sns_token' => $this->getSnsToken($corp, $persistentInfo),
        ]);
    }

    /**
     * 获取部门信息
     *
     * @param   $corp
     * @return  array   部门信息信息
     */
    public  function getDepartmentInfo($corp)
    {
        return  $this->_get('/department/list', [
            'access_token' => $this->getAccessToken($corp),
        ]);
    }

    /**
     * 获取部门成员信息
     *
     * @param $corp
     * @param $departmentId
     * @return mixed
     */
    public  function getUserByDepartment($corp, $departmentId)
    {
        return  $this->_get('/user/list', [
            'access_token'  => $this->getAccessToken($corp),
            'department_id' => $departmentId,
        ]);
    }

    /**
     * GET请求
     *
     * @param   string  $path   路径
     * @param   array   $params 参数
     * @return  mixed           结果
     */
    private function _get($path, $params)
    {

        $response   =$this->_request('GET',$path,[
            'query' => $params,
        ]);

        $result     = $this->_getResult($response->getBody()->getContents());

        if ($this->_isSuccess($result)) {

            return      $result;
        }

        $this->_makeApiException($result);
    }

    /**
     * POST请求
     *
     * @param   string  $path   路径
     * @param   array   $params 参数
     * @param   array   $data   数据
     * @return  mixed           结果
     */
    private function _post($path, $params, $data)
    {
        $response   =$this->_request('POST',$path,[
            'query' => $params,
            'body'  => json_encode($data, JSON_UNESCAPED_UNICODE),
        ]);

        $result     = $this->_getResult($response->getBody()->getContents());

        if ($this->_isSuccess($result)) {

            return      $result;
        }

        $this->_makeApiException($result);
    }

    /**
     * 获取路径
     *
     * @param   string  $path   路径
     * @return  string          url
     */
    private function _getDingUrl($path)
    {
        return  self::PREFIX_API . $path;
    }

    /**
     * 获取结果
     *
     * @param   string  $response   响应内容
     * @return  mixed               结果
     */
    private function _getResult($response)
    {
        return  json_decode($response, true);
    }

    /**
     * 是否成功
     *
     * @param   array   $result API访问结果
     * @result  bool            判断结果
     */
    private function _isSuccess($result)
    {
        return  0 == $result['errcode'];
    }

    /**
     * 抛出接口异常
     *
     * @param   array   $result API访问结果
     * @result  void
     * @thorws  \UnexpectedValueException
     */
    private function _makeApiException($result)
    {
        throw   new \UnexpectedValueException($result['errmsg'], $result['errcode']);
    }

    /**
     * 发送 http 请求
     *
     * @param $method
     * @param $path
     * @param array $options
     * @return mixed
     */
    private function _request($method, $path, $options = [])
    {
        $options['handler'] = $this->_getHttpHandler();

        return $this->_getHttpClient()->request($method, $this->_getDingUrl($path), $options);
    }

    /**
     * 获取 http 客户端
     *
     * @return null
     */
    private function _getHttpClient()
    {
        if ($this->_httpClient === null) {
            $this->_setHttpClient(new Client());
        }

        return $this->_httpClient;
    }

    /**
     * 设置 http 客户端
     *
     * @return null
     */
    private function _setHttpClient(ClientInterface $httpClient)
    {
        $this->_httpClient = $httpClient;

        return $this;
    }

    /**
     * 获取 http handler
     *
     * @return HandlerStack
     */
    private function _getHttpHandler()
    {
        if ($this->_httpHandler === null) {
            $this->_httpHandler = HandlerStack::create();
            $this->_registerMiddlewares($this->_httpHandler);
        }

        return $this->_httpHandler;
    }

    /**
     * 注册 http 中间服务
     *
     * @param HandlerStack $handler
     */
    private function _registerMiddlewares(HandlerStack $handler)
    {
        $handler->push($this->_logMiddleware());
    }

    /**
     * 日志 中间服务
     *
     * @return callable
     */
    private function _logMiddleware()
    {
        return Middleware::tap(function (RequestInterface $request, $options) {
            $this->logger &&
            $this->logger->debug("Request: {$request->getMethod()} {$request->getUri()}", [
                'headers'   => $request->getHeaders(),
                'body'      => $request->getMethod() == 'POST' ? (string)$request->getBody() : ''
            ]);
        });
    }

}