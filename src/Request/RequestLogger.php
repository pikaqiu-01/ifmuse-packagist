<?php

namespace   Kuandd\Common\Request;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class RequestLogger
{
    /**
     * @var     \Monolog\Logger
     */
    protected   $logger;

    /**
     * @var     array   配置项
     */
    protected   $config;

    /**
     * @var     string  日志文件路径
     */
    protected   $filePath;

    /**
     * Routes that will not log to file.
     *
     * All method to path like: admin/auth/logs*
     * or specific method to path like: get:admin/auth/logs
     */
    protected function getExcepts()
    {
        return [
        ];
    }

    /**
     * Foo constructor.
     */
    public function __construct()
    {
        $this->setConfig();
        $this->setFilePath();
        $this->initLogger();
    }

    /**
     * 读取配置信息
     */
    protected function setConfig()
    {
        $this->config = require_once __DIR__ . '/request-logger.php';
    }

    /**
     * 设置日志文件路径
     */
    protected function setFilePath()
    {
        switch ($this->config['type']) {
            case 'single' :
            default :
                $this->filePath = $this->config['path'] . '/request.log';
                break;
            case 'daily' :
                $this->filePath = $this->config['path'] . '/request-' . date('Y-m-d') . '.log';
                break;
        }
    }

    /**
     * 初始化logger
     */
    protected function initLogger()
    {
        $this->logger = new Logger($this->config['name']);
        $this->logger->pushHandler(new StreamHandler($this->filePath, Logger::INFO));
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        if ($this->shouldLogOperation($request)) {
            $log = [
                'path'    => $request->path(),
                'method'  => $request->method(),
                'ip'      => $request->getClientIp(),
                'user'    => $this->getUser($request),
                'input'   => $request->input(),
                'extra'   => $this->setExtra($request),
            ];

            $this->createLog($log);
        }

        return $next($request);
    }

    /**
     * 判断是否需要写入日志
     * @param Request $request
     *
     * @return bool
     */
    protected function shouldLogOperation(Request $request)
    {
        return $this->config['enable']
            && !$this->inExceptArray($request);
    }

    /**
     * Determine if the request has a URI that should pass through CSRF verification.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    protected function inExceptArray($request)
    {
        foreach ($this->getExcepts() as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            $methods = [];

            if (Str::contains($except, ':')) {
                list($methods, $except) = explode(':', $except);
                $methods = explode(',', $methods);
            }

            $methods = array_map('strtoupper', $methods);

            if ($request->is($except) &&
                (empty($methods) || in_array($request->method(), $methods))) {
                return true;
            }
        }

        return false;
    }

    /**
     * 获取用户名称
     *
     * @param Request $request
     * @return string
     */
    protected function getUser(Request $request)
    {
        return 'nobody';
    }

    /**
     * 设置自定义信息
     *
     * @param Request $request
     * @return null
     */
    protected function setExtra(Request $request)
    {
        return null;
    }

    /**
     * @param array $content
     */
    protected function createLog(array $content)
    {
        $row = "Path:{$content['path']} Method:{$content['method']} IP:{$content['ip']} User:{$content['user']} ";
        $row .= 'Input:'. json_encode($content['input']). " ";

        if(data_get($content, 'extra', null)) {
            $row .= 'Extra:' . json_encode($content['extra']). " ";
        }

        $this->logger->info($row);
    }
}