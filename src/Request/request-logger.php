<?php

return [

    'enable'    => env('REQUEST_LOG_ENABLE', true),

    'name'      => 'request',
    /** 日志组织方式 single: 单个文件, daily: 每天一个文件*/
    'type'      => env('REQUEST_LOG_TYPE', 'single'),

    'path'      => env('REQUEST_LOG_PATH', storage_path('logs')),
];