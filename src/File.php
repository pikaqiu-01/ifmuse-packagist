<?php
namespace   Kuandd\Common;

class File
{
    /**
     * 文件路径
     * @var  string
     */
    private $_path;

    /**
     * 资源
     * @var  resource
     */
    private $_resource;

    /**
     * 构造方法
     * @param  array  $path  文件路径列表
     */
    public function __construct($path)
    {
        if (!is_file($path)) {

            throw new \UnexpectedValueException('文件路径不存在!');
        }

        $this->_path = $path;
    }

    /**
     * 获取资源
     * @param  string  $mode  打开方式
     * @return resource       返回资源
     */
    public function getResource($mode)
    {
        if (!$this->_resource) {

            $this->_resource    = fopen($this->_path, $mode);
        }

        return  $this->_resource;
    }

    /**
     * 计算文件散列值
     * @param  string  $hashFunction  算法名称 md5|sha1
     * @return  string                文件散列值
     */
    public function getHash($hashFunction = 'md5')
    {
        $function       = $hashFunction . '_file';
        return  $function($this->_path);
    }
}