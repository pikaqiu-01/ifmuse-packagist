<?php

namespace Kuandd\Common;

use Illuminate\Support\ServiceProvider;
use Kuandd\Common\KDNiao\KDNiaoClient;

class KDNiaoServerProvider extends ServiceProvider
{

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // 发布配置文件
        $this->publishes([
            __DIR__.'/config/laravel-kdniao.php' => config_path('laravel-kdniao.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(KDNiaoClient::class, function ($app) {

            $config = [
                'businessId'    => config('laravel-kdniao.businessId'),
                'apiKey'        => config('laravel-kdniao.apiKey'),
                'logfile'       => config('laravel-kdniao.logfile'),
            ];

            return new KDNiaoClient($config);
        });
    }
}