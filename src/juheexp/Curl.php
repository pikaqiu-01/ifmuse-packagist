<?php

namespace Kuandd\Common\JuheExpress;

use Monolog\Logger;

class Curl
{
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Logger $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     *
     *  发送快递鸟物流信息查询请求
     *
     * @param $url
     * @param $data
     * @return mixed
     */
    public function request($url, $data)
    {
        $queryString = http_build_query($data);
        $realUrl = $url . '?'. $queryString;
        $header = [
            'Content-Type'  => 'application/x-www-form-urlencoded;charset=utf-8',
        ];
        $config = [
            'post'      => true,
            'header'    => $header,
        ];

        $result = json_decode($this->curlOpen($realUrl, $config), true);

        $this->throwExceptionIfNecessary($result, $data);

        return $result;
    }


    /**
     * 发送curl请求
     * @param $url
     * @param array $config
     * @return mixed
     */
    public function curlOpen($url, $config = array())
    {
        $arr = array('post' => false,'referer' => $url,'cookie' => '', 'useragent' => 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; customie8)', 'timeout' => 30, 'return' => true, 'proxy' => '', 'userpwd' => '', 'nobody' => false,'header'=>array(),'gzip'=>true,'ssl'=>false,'isupfile'=>false);
        $arr = array_merge($arr, $config);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, $arr['return']);
        curl_setopt($ch, CURLOPT_NOBODY, $arr['nobody']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $arr['useragent']);
        curl_setopt($ch, CURLOPT_REFERER, $arr['referer']);
        curl_setopt($ch, CURLOPT_TIMEOUT, $arr['timeout']);
        //curl_setopt($ch, CURLOPT_HEADER, true);
        if($arr['gzip']) curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        if($arr['ssl'])
        {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        if(!empty($arr['cookie']))
        {
            curl_setopt($ch, CURLOPT_COOKIEJAR, $arr['cookie']);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $arr['cookie']);
        }

        if(!empty($arr['proxy']))
        {
            //curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
            curl_setopt ($ch, CURLOPT_PROXY, $arr['proxy']);
            if(!empty($arr['userpwd']))
            {
                curl_setopt($ch,CURLOPT_PROXYUSERPWD,$arr['userpwd']);
            }
        }

        if(!empty($arr['header']['ip']))
        {
            array_push($arr['header'],'X-FORWARDED-FOR:'.$arr['header']['ip'],'CLIENT-IP:'.$arr['header']['ip']);
            unset($arr['header']['ip']);
        }
        $arr['header'] = array_filter($arr['header']);

        if(!empty($arr['header']))
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $arr['header']);
        }

        if ($arr['post'] != false)
        {
            curl_setopt($ch, CURLOPT_POST, true);
            if(is_array($arr['post']) && $arr['isupfile'] === false)
            {
                $post = http_build_query($arr['post']);
            }
            else
            {
                $post = $arr['post'];
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    /**
     * @param $result
     * @param $requestData
     * @throws ServiceException
     * @throws SystemException
     * @throws \Exception
     */
    private function throwExceptionIfNecessary($result, $requestData)
    {
        if(!is_array($result) || !array_key_exists('error_code', $result)) {

            $this->logger->addRecord(Logger::WARNING, '物流信息接口不可用', ['request' => $requestData, 'response' => $result]);

            throw new \Exception('物流信息接口不可用');
        }
        //系统级错误
        if ($result['error_code'] >= 10000 && $result['error_code'] < 200000) {

            $reason = isset($result['reason']) ? $result['reason'] : '物流接口参数错误';

            $this->logger->addRecord(Logger::WARNING, $reason, ['request' => $requestData, 'response' => $result]);

            throw new SystemException($reason);
        }
        //服务级错误
        if ($result['error_code'] >= 200000) {

            $reason = isset($result['reason']) ? $result['reason'] : '物流接口参数错误';

            $this->logger->addRecord(Logger::NOTICE, $reason, ['request' => $requestData, 'response' => $result]);

            throw new ServiceException($reason);
        }
    }

    /**
     * Adds a log record.
     *
     * @param  int     $level   The logging level
     * @param  string  $message The log message
     * @param  array   $context The log context
     *
     */
    private function addRecord($level, $message, array $context = array())
    {
        if (!$this->logger instanceof Logger) return;

        $this->addRecord($level, $message, $context);
    }
}