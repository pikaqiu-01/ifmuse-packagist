<?php

namespace Kuandd\Common\JuheExpress;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class JuheExpressClient
{
    /**
     * @var string  AppKey
     */
    private $appKey;

    /**
     * @var Logger  日志实例
     */
    private $logger;
    /**
     * ExpressClient constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        $this->setConfig($config);
    }

    /**
     * @param $number
     * @param $shippingCode
     * @return mixed
     */
    public function query($number, $shippingCode)
    {
        $requestData = [];
        $requestData['com']     = $shippingCode;
        $requestData['no']      = $number;
        $requestData['key']     = $this->appKey;
        $requestData['dtype']   = 'json';

        $queryUrl = 'http://v.juhe.cn/exp/index';

        return $this->getCurlClient()->request($queryUrl, $requestData);
    }

    /**
     * 获取curl查询实例
     *
     * @return Curl
     */
    protected function getCurlClient()
    {
        static $curl;

        if(!$curl instanceof Curl) {

            $curl = new Curl($this->logger);
        }

        return $curl;
    }

    /**
     * 初始化配置
     *
     * @param array $config
     * @throws \Exception
     */
    protected function setConfig(array $config)
    {
        if(!array_key_exists('appKey',$config))     throw new \Exception('AppKey不存在');
        if(!array_key_exists('logfile',$config))   throw new \Exception('日志目录未配置');

        $this->appKey       = $config['appKey'];

        $this->setLogger($config['logfile']);
    }

    /**
     * 初始化logger实例
     *
     * @param $logfile
     */
    protected function setLogger($logfile)
    {
        $this->logger = new Logger('juheexp', [new StreamHandler($logfile, Logger::DEBUG)]);
    }
}