<?php
/**
 * 请求转发
 */
namespace Kuandd\Common;

use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class RequestProxy
{
    /**
     * @var GuzzleClient
     */
    public $_guzzleClient;

    /**
     * 源请求对象
     */
    private $_sourceRequest;

    /**
     * 请求的URL
     */
    private $_requestUrl;

    /**
     * 请求选项
     */
    private $_options;

    /**
     * 响应对象
     */
    private $_response;

    /**
     * 排除的源请求header头
     */
    private $_exceptHeader = ['host'];

    /**
     * 构造方法
     *
     * @param $requestUrl               请求的目标URL
     * @param Request $sourceRequest    源请求对象
     */
    public function __construct($requestUrl, Request $sourceRequest)
    {
        $this->_guzzleClient = new GuzzleClient();
        $this->_sourceRequest = $sourceRequest;
        $this->_requestUrl = $requestUrl;
        $this->_initRequestOptions();
    }

    /**
     * 设置源请求对象
     *
     * @param Request $sourceRequest
     */
    public function setSourceRequest($sourceRequest)
    {
        $this->_sourceRequest = $sourceRequest;
    }

    /**
     * 设置请求URL
     *
     * @param $url
     */
    public function setRequestUrl($url)
    {
        $this->_requestUrl = $url;
    }

    /**
     * 发送请求
     *
     * @param string $method
     */
    public function execute($method = 'POST')
    {
        $this->_response = $this->_guzzleClient->request($method, $this->_requestUrl, $this->_options);
    }

    /**
     * 获取响应
     *
     * @return mixed
     */
    public function getResponse()
    {
        return  $this->_response;
    }

    /**
     * 获取请求头
     *
     * @return array
     */
    public function getRequestHeaders()
    {
        $headers = [];
        collect($this->_sourceRequest->headers->all())->map(function ($item, $key) use (& $headers) {

            if (in_array(strtolower($key), $this->_exceptHeader)) {

                return;
            }
            $headers[$key] = is_array($item) ? current($item) : null;
        });

        return  $headers;
    }

    /**
     * 获取请求选项
     *
     * @return mixed
     */
    public function getRequestOptions()
    {
        return  $this->_options;
    }

    /**
     * 初始化请求选项
     */
    private function _initRequestOptions()
    {
        $this->_options['headers'] = $this->getRequestHeaders();
        // 上传请求选项
        if ($this->_isUpload()) {

            unset($this->_options['headers']['content-type']);
            $uploadUrl = $this->_sourceRequest->header('upload-url');
            if (empty($uploadUrl)) {

                throw new \Exception('upload-url is not exists');
            }
            $this->setRequestUrl($uploadUrl);
            collect($this->_sourceRequest->all())->map(function ($item, $key) {

                if (is_array($item)) {

                    collect($item)->map(function ($value) use ($key) {

                        $part = [
                            'name' => $key . '[]',
                            'contents' => ($value instanceof UploadedFile) ? fopen($value->getRealPath(), 'r') : $value,
                        ];
                        if ($value instanceof UploadedFile) {

                            $part['filename'] = $value->getClientOriginalName();
                        }
                        $this->_options['multipart'][] = $part;
                    });
                } else {

                    $part = [
                        'name' => $key,
                        'contents' => ($item instanceof UploadedFile) ? fopen($item->getRealPath(), 'r') : $item,
                    ];
                    if ($item instanceof UploadedFile) {

                        $part['filename'] = $item->getClientOriginalName();
                    }
                    $this->_options['multipart'][] = $part;
                }
            });
        } else {

            $this->_options['body'] = $this->_sourceRequest->getContent();
        }
    }

    /**
     * 请求是否表单文件上传 (enctype=multipart/form-data)
     *
     * @return bool
     */
    private function _isUpload()
    {
        return false !== strpos($this->_sourceRequest->header('content-type'), 'multipart/form-data');
    }
}