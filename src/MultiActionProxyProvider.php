<?php

namespace   Kuandd\Common;

use Illuminate\Support\ServiceProvider;


class MultiActionProxyProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MultiActionProxy::class, function ($app) {

            return  MultiActionProxy::getInstance();
        });
    }

    /**
     * 获取容器
     *
     * @return  array   绑定容器类
     */
    public  function provides()
    {
        return  [MultiActionProxy::class];
    }
}
