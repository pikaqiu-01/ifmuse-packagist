<?php
namespace   Kuandd\Common;

/**
 * URL控制
 */
class   Url {

    /**
     * 信息
     */
    private $_info  = [];

    /**
     * QUERY_STRING参数
     */
    private $_query = [];

    /**
     * 构造函数
     *
     * @param   string  $url    URL
     */
    public  function __construct($url)
    {
        $this->_info    = parse_url($url);

        if (!empty($this->_info['query'])) {

            parse_str($this->_info['query'], $this->_query);
        }
    }

    /**
     * 生成URL
     *
     * @return  string  URL
     */
    public  function generate()
    {
        $scheme     = !empty($this->_info['scheme'])    ? $this->_info['scheme'] . ':'          : '';
        $user       = !empty($this->_info['user'])      ? $this->_info['user']                  : '';
        $pass       = !empty($this->_info['pass'])      ? ':' . $this->_info['pass']            : '';
        $at         = $user && $pass                    ? '@'                                   : '';
        $host       = !empty($this->_info['host'])      ? $this->_info['host']                  : '';
        $port       = !empty($this->_info['port'])      ? ':' . $this->_info['port']            : '';
        $path       = !empty($this->_info['path'])      ? $this->_info['path']                  : '';
        $query      = !empty($this->_query)             ? '?' . http_build_query($this->_query) : '';
        $fragment   = !empty($this->_info['fragment'])  ? '#' . $this->_info['fragment']        : '';

        return  $scheme . '//' .
            $user . $pass . $at .
            $host . $port . $path . $query .
            $fragment;
    }

    /**
     * 传入信息
     *
     * @param   string  $name   属性名
     * @param   mixed   $value  属性值
     */
    public  function __set($name, $value)
    {
        if ('query' == $name) {

            $this->_query   = $value;

            return  ;
        }

        $this->_info[$name] = $value;
    }

    /**
     * 获取信息
     *
     * @param   string  $name   属性名
     * @return  mixed           属性值
     */
    public  function __get($name)
    {
        if ('query' == $name) {

            return  $this->_query;
        }

        return  $this->_info[$name];
    }

    /**
     * 字符串化
     */
    public  function __toString()
    {
        return  $this->generate();
    }
}
