<?php
namespace   Kuandd\Common;

use \GuzzleHttp\Client;

class   OpClient {

    /**
     * 上传方法前缀
     */
    const       UPLOAD_METHOD_PREFIX    = 'upload';

    /**
     * 主机
     *
     * @var string
     */
    private $_host;

    /**
     * 协议
     *
     * @var     string
     * @example http|https
     */
    private $_proxy     = 'http';

    /**
     * 版本
     *
     * @var     string
     */
    private $_version   = 'v1';

    /**
     * 获取应用及公钥的回调函数
     *
     * @var callable
     */
    private $_keyCallback;

    /**
     * 构造函数
     *
     * @param   array   $options    配置数据
     */
    public  function __construct(array $options)
    {
        $this->_host        = $options['host'];
        $this->_proxy       = isset($options['proxy']) && in_array($options['proxy'], ['http','https']) ? $options['proxy'] : 'http';
        $this->_version     = isset($options['version'])    ? $options['version']   : 'v1';
        $this->_keyCallback = $options['callback'];
    }

    /**
     * 魔术方法：实例方法调用
     *
     * @param   string  $name       方法名
     * @param   array   $arguments  参数列表
     * @return  array               返回数据
     */
    public  function __call($name, $arguments)
    {

        if ($this->_isUpload($name)) {

            $path = $this->_getPathByName(substr($name, strlen(self::UPLOAD_METHOD_PREFIX)));
            return  $this->_upload($path, $arguments[0]);
        }

        $path   = $this->_getPathByName($name);

        return  $this->_post($path, $arguments[0]);
    }

    /**
     * 是否为上传
     * @param  string  $name  方法名
     * @return boolean        判断结果
     */
    private function _isUpload($name)
    {
        return 0 === strpos($name, self::UPLOAD_METHOD_PREFIX);
    }

    /**
     * 根据方法名获取接口路径
     *
     * @param   string  $name   方法名
     * @return  string          接口路径
     */
    private function _getPathByName($name)
    {
        $clips      = [];
        $match      = preg_match_all('~\w+?[a-z0-9](?=[A-Z]|$)~', $name, $clips);

        if (0 === $match) {

            throw   new \UnexpectedValueException('method [' . self::class . '::' . $name . '] not exists', -1);
        }

        $clipsName  = array_filter(array_map('trim', $clips[0]));

        return      '/api/' . $this->_version . '/' .implode('/', array_map('strtolower', $clipsName));
    }

    /**
     * POST请求
     *
     * @param   string  $path   路径
     * @param   array   $data   数据
     * @return  mixed           结果
     */
    private function _post($path, $data)
    {
        $url        = $this->_proxy . '://' . $this->_host . $path;
        $client     = new Client;
        $this->_sign($data);
        $response   = $client->request('POST', $url, [
            'body'      => json_encode($data),
            'headers'   => [
                'Content-type'  => 'application/json',
            ],
        ]);
        $result     = $this->_getResult($response->getBody()->getContents());

        if ($this->_isSuccess($result)) {

            return      $result;
        }

        $this->_makeApiException($result);
    }

    /**
     * 上传请求
     * 
     * @param  string  $path  路径
     * @param  array   $data  数据
     * @return mixed          结果
     */
    private function _upload($path, $data)
    {
        $url            = $this->_proxy . '://' . $this->_host . $path;
        $client         = new Client;
        $dataForSign    = [];
        foreach ($data as $field => $item) {

            if ($item instanceof File) {

                $dataForSign[$field]    = $item->getHash('md5');
            } else {

                $dataForSign[$field]    = $item;
            }
        }
        $this->_sign($dataForSign);
        $options    = $this->_uploadConvertData(array_merge($dataForSign, $data));
        $response   = $client->request('POST', $url, $options);
        $result     = $this->_getResult($response->getBody()->getContents());

        if ($this->_isSuccess($result)) {

            return      $result;
        }

        $this->_makeApiException($result);
    }

    /**
     * 组合上传数据
     * @param  array  $data  待组合数据
     * @return  array        组全完成数据
     */
    private     function    _uploadConvertData($data)
    {
        $options  = [];
        foreach ($data as $key => $value) {

            if ($value instanceof File) {

                $options['multipart'][]     = [
                    'name'      => $key,
                    'contents'  => $value->getResource('rb'),
                ];
            } else {

                $options['multipart'][]     = [
                    'name'      => $key,
                    'contents'  => $value,
                ];
            }
        }
        return  $options;
    }

    /**
     * 签名
     *
     * @param   array   $data   数据引用
     */
    private function _sign(& $data)
    {
        list($key, $publicKey)  = call_user_func($this->_keyCallback);
        $data       += [
            'plateformKey'  => $key,
            '_onceStr'      => microtime(true),
        ];
        $sorted     = $data;
        $this->_sort($sorted);
        $signature  = sha1(http_build_query($sorted) . $publicKey);
        $data       += [
            'signature' => $signature,
        ];
    }

    /**
     * 按键做递归排序
     *
     * @param   mixed   $data   待排序的数据引用
     */
    private function _sort(& $data)
    {
        if (!is_array($data)) {

            return  ;
        }

        foreach ($data as $key => $value) {

            $this->_sort($data[$key]);
        }

        ksort($data, SORT_NATURAL);
    }

    /**
     * 获取结果
     *
     * @param   string  $response   响应内容
     * @return  mixed               结果
     */
    private function _getResult($response)
    {
        return  json_decode($response, true);
    }

    /**
     * 是否成功
     *
     * @param   array   $result API访问结果
     * @result  bool            判断结果
     */
    private function _isSuccess($result)
    {
        return  isset($result['code']) && 0 == $result['code'];
    }

    /**
     * 抛出接口异常
     *
     * @param   array   $result API访问结果
     * @result  void
     * @thorws  \UnexpectedValueException
     */
    private function _makeApiException($result)
    {
        throw   new \UnexpectedValueException($result['message'], $result['code']);
    }

}
