<?php

namespace Kuandd\Common\KDNiao;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class KDNiaoClient
{
    /**
     * @var string  商户ID
     */
    private $businessId;

    /**
     * @var string  API key
     */
    private $apiKey;

    /**
     * @var string 快递鸟物流查询接口
     */
    private $queryUrl = 'http://api.kdniao.cc/Ebusiness/EbusinessOrderHandle.aspx';

    /**
     * @var Logger  日志实例
     */
    private $logger;

    const RT_INSTANT_QUERY          = '1002';       //即时查询
    const RT_RECOGNIZE_SHIPPER      = '2002';       //单号识别


    /**
     * ExpressClient constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        $this->setConfig($config);
    }

    /**
     * 单号识别
     *
     * @param $number string 物流单号
     * @return array  {"LogisticCode":"240620885237","Shippers":[{"ShipperName":"顺丰快递","ShipperCode":"SF"}],"EBusinessID":"1383157","Code":"100","Success":true}
     */
    public function recognizeShipper($number)
    {
        $requestData = [
            'LogisticCode' => $number,
        ];
        $requestData = json_encode($requestData);

        $data = array(
            'EBusinessID' => $this->businessId,
            'RequestType' => self::RT_RECOGNIZE_SHIPPER,
            'RequestData' => urlencode($requestData) ,
            'DataType' => '2',
            'DataSign' => $this->encrypt($requestData, $this->apiKey)
        );

        return $this->getCurlClient()->request($this->queryUrl, $data);
    }

    /**
     * @param $number
     * @param $shipperCode
     * @param null $orderCode
     * @return mixed
     */
    public function query($number, $shipperCode, $orderCode = null)
    {
        $requestData = [
            'LogisticCode'  => $number,
            'ShipperCode'   => $shipperCode,
        ];
        if(!is_null($orderCode)) {
            array_push($requestData, $orderCode);
        }
        $requestData = json_encode($requestData);

        $data = array(
            'EBusinessID' => $this->businessId,
            'RequestType' => self::RT_INSTANT_QUERY,
            'RequestData' => urlencode($requestData) ,
            'DataType' => '2',
            'DataSign' => $this->encrypt($requestData, $this->apiKey)
        );

        return $this->getCurlClient()->request($this->queryUrl, $data);
    }

    /**
     * 获取curl查询实例
     *
     * @return Curl
     */
    protected function getCurlClient()
    {
        static $curl;

        if(!$curl instanceof Curl) {

            $curl = new Curl($this->logger);
        }

        return $curl;
    }

    /**
     * 电商Sign签名生成
     *
     * @param $data
     * @param $apiKey
     * @return string
     */
    protected function encrypt($data, $apiKey) {

        return urlencode(base64_encode(md5($data.$apiKey)));
    }

    /**
     * 初始化配置
     *
     * @param array $config
     * @throws \Exception
     */
    protected function setConfig(array $config)
    {
        if(!array_key_exists('businessId',$config)) throw new \Exception('商户ID不存在');
        if(!array_key_exists('apiKey',$config))     throw new \Exception('API key不存在');
        if(!array_key_exists('logfile',$config))   throw new \Exception('错误日志目录未配置');

        $this->businessId   = $config['businessId'];
        $this->apiKey       = $config['apiKey'];

        $this->setLogger($config['logfile']);
    }

    /**
     * 初始化logger实例
     *
     * @param $logfile
     */
    protected function setLogger($logfile)
    {
        $this->logger = new Logger('kdniao', [new StreamHandler($logfile, Logger::DEBUG)]);
    }
}