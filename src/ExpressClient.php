<?php

namespace Kuandd\Common;


/**
 * 阿里云全国快递物流查询接口客户端
 *
 * Class ExpressClient
 * @package Kuandd\Common
 */
class ExpressClient
{
    /**
     * @var appKey
     */
    private $appKey;

    /**
     * @var appSecret
     */
    private $appSecret;

    /**
     * @var appCode
     */
    private $appCode;

    /**
     *
     */
    const HOST = 'http://jisukdcx.market.alicloudapi.com';

    /**
     * 快递查询接口调用地址
     */
    const EXPRESS_QUERY_PATH = self::HOST.'/express/query';

    /**
     * 获取快递公司接口调用地址
     */
    const EXPRESS_TYPE_PATH = self::HOST.'/express/type';

    const SUCCESS                   = 0;        //success
    const E_EMPTY_NUMBER            = 201;      //快递单号为空
    const E_EMPTY_COMPANY           = 202;      //快递公司为空
    const E_COMPANY_NOT_EXIST       = 203;      //快递公司不存在
    const E_COMPANY_IDENTIFY_FAILURE= 204;      //快递公司识别失败
    const E_NO_INFORMATION          = 205;      //没有信息
    const E_TOO_MANY_FAIL_QUERY     = 207;      //快递单号错误次数过多

    /**
     * ExpressClient constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        if(!array_key_exists('appKey',$config)) throw new \Exception('appKey不存在');
        if(!array_key_exists('appSecret',$config)) throw new \Exception('appSecret不存在');
        if(!array_key_exists('appCode',$config)) throw new \Exception('appCode不存在');

        $this->appKey       = $config['appKey'];
        $this->appSecret    = $config['appSecret'];
        $this->appCode      = $config['appCode'];
    }

    /**
     * 查询物流信息
     * @param $number
     * @param string $type
     * @throws \Exception
     * @return array
     */
    public function query($number,$type = 'auto')
    {
        $header = [
            'Authorization' => "Authorization:APPCODE {$this->appCode}",
        ];

        $result = $this->curlOpen(self::EXPRESS_QUERY_PATH."?number=$number&type=$type", ['header'=>$header]);
        if(!$result) {
            throw new \Exception('查询失败,请检查appCode是否合法');
        }
        return json_decode($result,true);
    }

    /**
     * 查询快递公司信息
     * @return array
     * @throws \Exception
     */
    public function type()
    {
        $header = [
            'Authorization' => "Authorization:APPCODE {$this->appCode}",
        ];

        $result = $this->curlOpen(self::EXPRESS_TYPE_PATH, ['header'=>$header]);
        if(!$result) {
            throw new \Exception('查询失败,请检查appCode是否合法');
        }
        return json_decode($result,true);
    }

    /**
     * 物流信息接口错误码
     *
     * @return array
     */
    public function errorCodes()
    {
        return [
            self::SUCCESS                       => 'ok',
            self::E_EMPTY_NUMBER                => '快递单号为空',
            self::E_EMPTY_COMPANY               => '快递公司为空',
            self::E_COMPANY_NOT_EXIST           => '快递公司不存在',
            self::E_COMPANY_IDENTIFY_FAILURE    => '快递公司识别失败',
            self::E_NO_INFORMATION              => '没有信息',
            self::E_TOO_MANY_FAIL_QUERY         => '快递单号错误次数过多',
        ];
    }

    /**
     * 发送curl请求
     * @param $url
     * @param array $config
     * @return mixed
     */
    protected function curlOpen($url, $config = array())
    {
        $arr = array('post' => false,'referer' => $url,'cookie' => '', 'useragent' => 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; customie8)', 'timeout' => 20, 'return' => true, 'proxy' => '', 'userpwd' => '', 'nobody' => false,'header'=>array(),'gzip'=>true,'ssl'=>false,'isupfile'=>false);
        $arr = array_merge($arr, $config);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, $arr['return']);
        curl_setopt($ch, CURLOPT_NOBODY, $arr['nobody']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $arr['useragent']);
        curl_setopt($ch, CURLOPT_REFERER, $arr['referer']);
        curl_setopt($ch, CURLOPT_TIMEOUT, $arr['timeout']);
        //curl_setopt($ch, CURLOPT_HEADER, true);
        if($arr['gzip']) curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        if($arr['ssl'])
        {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        if(!empty($arr['cookie']))
        {
            curl_setopt($ch, CURLOPT_COOKIEJAR, $arr['cookie']);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $arr['cookie']);
        }

        if(!empty($arr['proxy']))
        {
            //curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
            curl_setopt ($ch, CURLOPT_PROXY, $arr['proxy']);
            if(!empty($arr['userpwd']))
            {
                curl_setopt($ch,CURLOPT_PROXYUSERPWD,$arr['userpwd']);
            }
        }

        if(!empty($arr['header']['ip']))
        {
            array_push($arr['header'],'X-FORWARDED-FOR:'.$arr['header']['ip'],'CLIENT-IP:'.$arr['header']['ip']);
            unset($arr['header']['ip']);
        }
        $arr['header'] = array_filter($arr['header']);

        if(!empty($arr['header']))
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $arr['header']);
        }

        if ($arr['post'] != false)
        {
            curl_setopt($ch, CURLOPT_POST, true);
            if(is_array($arr['post']) && $arr['isupfile'] === false)
            {
                $post = http_build_query($arr['post']);
            }
            else
            {
                $post = $arr['post'];
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }
}

