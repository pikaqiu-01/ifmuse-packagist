<?php

namespace Kuandd\Common;

use Illuminate\Support\ServiceProvider;
use Kuandd\Common\JuheExpress\JuheExpressClient;

class JuheExpressServerProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // 发布配置文件
        $this->publishes([
            __DIR__.'/config/laravel-juheexp.php' => config_path('laravel-juheexp.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(JuheExpressClient::class, function ($app) {

            $config = [
                'appKey'        => config('laravel-juheexp.appKey'),
                'logfile'       => config('laravel-juheexp.logfile'),
            ];

            return new JuheExpressClient($config);
        });
    }
}