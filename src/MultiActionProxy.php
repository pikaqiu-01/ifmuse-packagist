<?php
namespace   Kuandd\Common;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Illuminate\Routing\RouteCollection;
use Illuminate\Validation\ValidationException;
use Illuminate\Routing\Pipeline;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class   MultiActionProxy {

    /**
     * 默认最大调用次数
     */
    const   MAX_CALLS_DEFAULT   = 10;

    /**
     * 实例
     */
    private static  $_instance;

    /**
     * 单次最大调用控制器次数
     */
    private   $_maxCalls        = self::MAX_CALLS_DEFAULT;

    /**
     * 获取实例
     *
     * @param   int $maxCalls                   最大调用数量
     * @return  Kuandd\Common\MultiActionProxy  本类实例
     */
    public  static  function getInstance($maxCalls = self::MAX_CALLS_DEFAULT)
    {
        if (!(self::$_instance instanceof self)) {

            self::$_instance    = new self();
            self::$_instance->setCallLimit($maxCalls);
        }

        return  self::$_instance;
    }

    /**
     * 禁用克隆实例
     */
    private function __clone()
    {
    }

    /**
     * 构造函数
     */
    private function __construct()
    {
    }

    /**
     * 修改控制器调用次数限制
     *
     * @param   int     $count  新前缀|为空不更改|默认为空
     */
    public  function setCallLimit($count)
    {
        $this->_maxCalls    = $count;
    }

    /**
     * 校验
     *
     * @param   Request $request    请求
     * @return  void
     * @thorws  Exception           校验失败抛出异常
     */
    public  function verify(Request $request)
    {
        $listAction     = array_keys($request->all());
        $this->_verifyMaxCalls($listAction);

        foreach ($listAction as $action) {

            $this->_verifyActionExpression($action);
        }
    }

    /**
     * 执行
     *
     * @param   Illuminate\Http\Request             $request    请求
     * @param   Illuminate\Routing\RouteCollection  $listRoute  路由集合
     * @param   Illuminate\Routing\Router           $router     路由实例
     * @return  array                                           执行结果
     */
    public  function invoke(Request $request, RouteCollection $listRoute, Router $router)
    {
        $resultAll  = [];
        $requestAll = $request->all();
        $count  = $listRoute->count();

        foreach ($requestAll as $action => $params) {

            if ($route  = $listRoute->getByName($action)) {

                $currentRequest = $request->duplicate();
                $currentRequest->json()->replace($params);

                $request->setRouteResolver(function() use($route) { return $route; });
                $currentRequest->setRouteResolver(function() use($route) { return $route; });

                // 生成当前request 经过中间件过滤
                $content        = self::_throughMiddleware($currentRequest, $route, $router);

                // 代理路由过程中抛错的情况
                if ($content instanceof ValidationException) {

                    $resultAll[$action] = [
                        'code'    => 3000001,
                        'message' => $content->validator->getMessageBag(),
                    ];
                }elseif ($content instanceof \Exception){

                    $resultAll[$action] = [
                        'code'    => $content->getCode()    ? $content->getCode()       : -1,
                        'message' => $content->getMessage() ? $content->getMessage()    : 'exception unknown',
                    ];
                 // 正常情况
                } else {

                    $resultAll[$action] = [
                        'code'    => 0,
                        'message' => 'OK',
                        'data'    => json_decode($content->content(), true),
                    ];
                }


             // 路由错误
            } else {
                $resultAll[$action]    = [
                    'code'      => 1000001,
                    'message'   => 'action invalid',
                ];
            }
        }

        return  $resultAll;
    }

    /**
     * request经过中间件过滤
     *
     * @param Request $request
     * @param Route $route
     * @param Router $router
     * @return mixed
     */
    protected function _throughMiddleware(Request $request, Route $route, Router $router) {

        $listMiddleware = collect($router->getMiddleware())->only($route->middleware())->toArray();

        return  (new Pipeline($GLOBALS['app']))
                ->send($request)
                ->through($GLOBALS['app']->shouldSkipMiddleware() ? [] : $listMiddleware)
                ->then(function ($request) use ($route) {

                    $response = $route->bind($request)->run();

                    if ($response instanceof PsrResponseInterface) {

                        $response = (new HttpFoundationFactory)->createResponse($response);
                    } elseif (! $response instanceof SymfonyResponse) {

                        $response = new Response($response);
                    }

                    return $response->prepare($request);
                });
    }

    /**
     * @param $listAction
     * @throws \Exception
     */
    private function _verifyMaxCalls($listAction)
    {
        if ($this->_maxCalls < count($listAction)) {

            throw   new \Exception('too many actions', 1000002);
        }
    }

    /**
     * @param $action
     */
    private function _verifyActionExpression($action)
    {
    }
}

