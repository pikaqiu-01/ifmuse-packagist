<?php

return [
    'businessId'    => env('KDNIAO_BUSINESS_ID','your businessID'),
    'apiKey'        => env('KDNIAO_API_KEY', ' your API Key'),
    'logfile'       => storage_path(env('KDNIAO_LOG_FILE', 'logs/express.log')),
];