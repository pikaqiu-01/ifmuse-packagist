<?php

return [
    'appKey'        => env('JUHEEXP_APP_KEY', 'your appKey'),
    'logfile'       => storage_path(env('JUHEEXP_LOG_FILE', 'logs/express.log')),
];